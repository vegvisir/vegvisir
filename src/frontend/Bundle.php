<?php 

	/*
		This file generates a minified bundle of all resources required to load the Vegvisir front-end. It is usually invoked by calling VV::init() 
	*/

	namespace Vegvisir\Frontend;

	use VV;
	use Vegvisir\Kernel\ENV;
	use Vegvisir\Kernel\Path;

	// Export environment variables for use in JavaScript
	$EXPORT = [
		"REQUEST_OPTIONS"          => [
			"REQUEST_METHOD" => $_SERVER["REQUEST_METHOD"],
			"REQUEST_BODY"   => $_SERVER["CONTENT_TYPE"] === "application/x-www-form-urlencoded"
				? http_build_query($_POST)
				: null
		],
		ENV::WORKER_PATHNAME->name => ENV::get(ENV::WORKER_PATHNAME)
	];

?>

<script>
<?= \Vegvisir\Kernel\LICENSE_HEADER_START ?>
<?php // Initialize Vegvisir global object with exported environment variables ?>
globalThis.vegvisir = <?= json_encode((object) $EXPORT) ?>;
<?php // Alias for globalThis.vegvisir ?>
globalThis.vv = globalThis.vegvisir;

<?php // Load front-end modules ?>
<?= VV::js(Path::vegvisir("src/frontend/js/navigation/Controller.js"), false) ?>;
<?= VV::js(Path::vegvisir("src/frontend/js/navigation/Element.js"), false) ?>;

<?php // Finally, load the front-end initializer ?>
<?= VV::js(Path::vegvisir("src/frontend/js/Vegvisir.js"), false) ?>
<?= \Vegvisir\Kernel\LICENSE_HEADER_END ?>
</script>