const SOFTNAV_ENABLED_HEADER = "X-Vegvisir-Navigation";

class NavigationEvent {
	fetchOptions = {
		headers: {}
	};

	/**
	 * Create a new Vegvisir soft navigation
	 * @param {MessageEvent} event 
	 */
	constructor(event) {
		this.id = event.data.requestId;
		this.url = event.data.requestUrl;

		// Set softnav enabled header
		this.fetchOptions.headers[SOFTNAV_ENABLED_HEADER] = true;
		
		if (event.data.requestOptions) {
			// Carry request method from inital request
			this.fetchOptions.method = event.data.requestOptions.REQUEST_METHOD;
	
			// Carry request body from initial request
			if (event.data.requestOptions.REQUEST_BODY) {
				this.fetchOptions.headers["Content-Type"] = "application/x-www-form-urlencoded";
				this.fetchOptions.body = event.data.requestOptions.REQUEST_BODY;
			}
		}

		this.#navigate();
	}

	async #navigate() {
		const response = await fetch(new Request(this.url, this.fetchOptions));
		
		// Output page HTML with shell id as target
		return globalThis.postMessage({
			requestId: this.id,
			responseStatus: response.status,
			responseBody: await response.text()
		});
	}
}

globalThis.onmessage = (event) => new NavigationEvent(event);