class HTMLVegvisirShellElement extends HTMLElement {
	constructor() {
		super();

		this.setLoading(false);
	}

	/**
	 * 
	 * @param {Boolean} state 
	 */
	setLoading(state = true) {
		this.setAttribute("vv-loading", state);
	}

	/**
	 * 
	 * @param {URL|String|null} url 
	 * @param {vegvisir.Navigation.POSITION} position 
	 * @param {vegvisir.Navigation.MODE} mode 
	 * @returns 
	 */
	async navigate(url, position, mode) {
		return await (new vegvisir.Navigation(url)).navigate(this, position, mode);
	}
}

window.customElements.define("vv-shell", HTMLVegvisirShellElement);