<?php

	namespace Vegvisir\Kernel;

	// Licence headers for LibreJS etc.
	const LICENSE_HEADER_START = "// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later" . PHP_EOL;
	const LICENSE_HEADER_END = PHP_EOL . "// @license-end";