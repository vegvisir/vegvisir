<?php

	use Vegvisir\Kernel\ENV;
	use Vegvisir\Kernel\Path;
	use Vegvisir\Kernel\Format;
	use Vegvisir\Request\Controller;

	use vlw\GlobalSnapshot;
	use MatthiasMullie\Minify;

	class VV extends Path {
		// Raise an error if file at pathname is not found
		private static function is_file(string $pathname): bool {
			return !is_file($pathname) ? !trigger_error("File '{$pathname}' not found.", E_USER_WARNING) : true;
		}

		// Set HTTP response code and return error page if enabled
		protected static function error(int $code): void {
			http_response_code($code);

			if (!ENV::isset(ENV::SITE_ERROR_PAGE) || !Controller::client_accepts_html()) {
				die();
			}

			die(self::include(ENV::get(ENV::SITE_ERROR_PAGE)));
		}

		// Load and return minified CSS from file
		public static function css(string $pathname, bool $relative = true): string {
			$pathname = $relative ? Path::root(Format::str_append_extension($pathname, ".css")) : $pathname;

			// Import and minify CSS stylesheet or return empty string if not found
			return self::is_file($pathname) ? (new Minify\CSS($pathname))->minify() : "";
		}

		// Load and return minified JS from file
		public static function js(string $pathname, bool $relative = true): string {
			$pathname = $relative ? Path::root(Format::str_append_extension($pathname, ".js")) : $pathname;

			// Import and minify JS source or return empty string if not found
			return self::is_file($pathname) ? (new Minify\JS($pathname))->minify() : "";
		}

		// Load and return contents of a file
		public static function embed(string $pathname, bool $relative = true): string {
			$pathname = $relative ? Path::root($pathname) : $pathname;
			
			return self::is_file($pathname) ? file_get_contents($pathname) : "";
		}

		// Include a PHP file from absolute path or from root of user context
		public static function include(string $path, bool $relative = true) {
			$snapshot = new GlobalSnapshot();

			// Destruct pathname and query from path
			[$pathname, $query] = strpos($path, "?") ? explode("?", $path, 2) : [$path, null];
			// Load PHP file relative from user context root
			$pathname = $relative ? parent::root(Format::str_append_extension($pathname, ".php")) : Format::str_append_extension($pathname, ".php");

			// Return an empty string if file is not found so exection can continue
			if (!self::is_file($pathname)) {
				return "";
			}

			// Set search parameters before import if defined
			if ($query) {
				$snapshot->capture();
				
				parse_str($query, $pieces);
				foreach ($pieces as $k => $v) {
					$_GET[$k] = $v;
				}
			}

			// Import and evaluate PHP file
			include $pathname;

			// Restore superglobals if captured
			if ($snapshot->captured) {
				$snapshot->restore();
			}
		}

		// Bundle resources required to load the Vegvisir front-end
		public static function init(): void {
			include Path::vegvisir("src/frontend/Bundle.php");
		}
	}