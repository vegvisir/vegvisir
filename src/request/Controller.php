<?php

	namespace Vegvisir\Request;

	const SOFTNAV_ENABLED_HEADER = "HTTP_X_VEGVISIR_NAVIGATION";

	class Controller {
		// Returns true if the client sent an HTTP Accept header which includes text/html
		public static function client_accepts_html(): bool {
			if ($_SERVER["HTTP_ACCEPT"] === "*/*") {
				return true;
			}
			
			return strpos($_SERVER["HTTP_ACCEPT"] ?? "", "text/html") !== false;
		}

		// Returns true if the client sent a SOFTNAV_ENABLED_HEADER
		public static function is_softnav_enabled(): bool {
			return !empty($_SERVER[SOFTNAV_ENABLED_HEADER]);
		}
	}