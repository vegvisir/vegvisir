<?php

	namespace Vegvisir\Support;

	use Vegvisir\Kernel\ENV;

	class MimeType {
		private const DEFAULT_TYPE = "application/x-empty";
		private const CACHE_FILENAME = "php_vegvisir_types";

		private readonly \stdClass $cache;

		public function __construct() {
			// Fetch and cache MIME-types if cache file does not exist
			if (!is_file(self::get_cache_file_path())) {
				$this->init_cache();
			}

			// Parse MIME-types from cache file
			$this->cache = json_decode(file_get_contents(self::get_cache_file_path()));
		}

		// Return an absolute pathname to the cache file
		private static function get_cache_file_path(): string {
			return sys_get_temp_dir() . "/" . self::CACHE_FILENAME;
		}

		// Parse an RFC 4288-compatible MIME-type list from URL set in environment variable
		private function fetch_types(): array {
			// Courtesy of https://www.php.net/manual/en/function.mime-content-type.php#107798
			// Modified slightly for readability and assoc array $s[ext] => [mime] instead of a text output
			$s = array();
			foreach (@explode("\n", @file_get_contents(ENV::get(ENV::RFC_4288_URL))) as $x){
				if (isset($x[0]) && $x[0] !== '#' && preg_match_all('#([^\s]+)#', $x, $out) && isset($out[1]) && ($c = count($out[1])) > 1) {	
					for($i = 1; $i < $c; $i++) {
						$s[$out[1][$i]] = $out[1][0];
					}
				}
			}

			return $s;
		}

		// Create, fetch, and cache MIME-types
		private function init_cache() {
			// Create cache file
			tempnam(sys_get_temp_dir(), self::CACHE_FILENAME);

			// Fetch and write MIME-type as json to cache file
			$cache = fopen(self::get_cache_file_path(), "w");
			fwrite($cache, json_encode($this->fetch_types()));
			fclose($cache);
		}

		public function get_type_from_ext(string $ext): string {
			return $this->cache->$ext ?? self::DEFAULT_TYPE;
		}

		public function get_type_from_file(string $path): string {
			return $this->get_type_from_ext(pathinfo($path, PATHINFO_EXTENSION));
		}
	}