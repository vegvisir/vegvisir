<p align="center">
<img width="100" src="https://blob.vlw.se/019204b6-0225-73ad-9aea-8c465b0e4e99">
</p>
<h1 align="center">Vegvisir</h1>
<p align="center">Vegvisir is a web navigation framework written in and for PHP and JavaScript developers.</p>
<p align="center"><a href="https://vegvisir.vlw.se/"><strong>Check out the Vegvisir website for more information</strong></a></p>

<h2 align="center">Key Features</h2>

- **Framework and project**: Vegvisir is kept in a separate directory from your project.
- [**Asset injection**](https://vegvisir.vlw.se/docs/API/PHP/VV/css): Bundle your page CSS and JS into the same HTTP response.
- **Multi-threaded**: Requests are loaded and processed in a Worker-thread.
- **Native PHP templating**: Templating with PHP's ["alternative syntax for control structures"](https://www.php.net/manual/en/control-structures.alternative-syntax.php)
- **Soft-navigation**: Automatic soft-navigation between pages and [shells](https://vegvisir.vlw.se/docs/API/PHP/VV/shell).
- **Imports**: [Re-use PHP modules as snippets on multiple pages with imports](https://vegvisir.vlw.se/docs/API/PHP/VV/include).
- **Soft-navigation of any element**: Load pages/modules into a specific HTMLElement with [`targetElement` position syntax](https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentElement#position).

# Documentation

[You can find documentation on the Vegvisir website](https://vegvisir.vlw.se/docs)

# Support

<table>
  <thead>
    <tr>
      <th rowspan="2">PHP</th>
      <th colspan="3">Browsers</th>
    </tr>
    <tr align="center">
      <th>Chromium</th>
      <th>Gecko</th>
      <th>WebKit</th>
    </tr>
  </thead>
  <tbody>
    <tr align="center">
      <td><img src="https://github.com/VictorWesterlund/vegvisir/assets/35688133/a1a78138-5cef-4ba1-8dca-928eb32ebe9d"/></td>
      <td><img src="https://user-images.githubusercontent.com/35688133/230028928-dca1467d-8c63-4e69-9524-78e5751eaf24.png"/></td>
      <td><img src="https://user-images.githubusercontent.com/35688133/230029200-624d0126-9640-4b78-9eb5-a2e4be4e51be.png"/></td>
      <td><img src="https://user-images.githubusercontent.com/35688133/230029381-e7162ba1-e9ef-4b34-803f-043b5d16d365.png"/></td>
    </tr>
    <tr>
      <td>✅ Version 8.2+</td>
      <td>✅ Version 80+</td>
      <td>✅ Version 75+</td>
      <td>✅ Version 14.1+</td>
    </tr>
  </tbody>
</table>
